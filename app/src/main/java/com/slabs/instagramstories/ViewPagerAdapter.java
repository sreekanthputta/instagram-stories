package com.slabs.instagramstories;

import android.app.Activity;
import android.content.Context;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sreekanth Putta on 04-05-2017.
 */
public class ViewPagerAdapter extends PagerAdapter {
    Activity activity;
    List<PlatformData> data;
    LayoutInflater layoutInflater;
    Context context;
    private float max;
    private float progress;
    private final ArrayList<ArrayList<RoundCornerProgressBar>> Progressbars;
    private final ArrayList<ArrayList<Integer>> images;
    ArrayList<View> registeredFragments = new ArrayList<View>();


    public ViewPagerAdapter(Context context, Activity activity, List<PlatformData> data) {
        this.activity = activity;
        this.data = data;
        this.context = context;
        this.Progressbars =  new ArrayList<ArrayList<RoundCornerProgressBar>>();
        this.images = new  ArrayList<ArrayList<Integer>>();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        Log.d("and", "instantiateItem called" + position);
        layoutInflater = (LayoutInflater)activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View itemView = layoutInflater.inflate(R.layout.viewpager_item,container,false);
        registeredFragments.add(position, itemView);
        LinearLayout lin = (LinearLayout) itemView.findViewById(R.id.linearLayoutitem);
        ImageView imageView = (ImageView)registeredFragments.get(position).findViewById(R.id.imageView);

        images.add(position,data.get(position).getImages());
        int length = images.get(position).size();
        imageView.setImageResource(images.get(position).get(0));

        final ArrayList<RoundCornerProgressBar> progress = new ArrayList<RoundCornerProgressBar>();

        final ViewPager viewPager = ((ViewPager) container);

        CountDownTimer countDownTimer = new CountDownTimer(3000, 10) {
            ArrayList<Integer> imgs = images.get(position);

            int length = imgs.size();
            int count = 0;
            ImageView imageView = (ImageView)registeredFragments.get(position).findViewById(R.id.imageView);

            @Override
            public void onTick(long millisUntilFinished_) {
                Double perc = 100.0;
                double progress = (3000 - millisUntilFinished_) / 3000.0 * perc;
                Progressbars.get(position).get(count).setProgress((float) progress);
              //  Log.e("CF", millisUntilFinished_ + "j" + progress);
            }

            @Override
            public void onFinish() {

                for(Integer i : imgs) {
                    Log.d("And", "image no:" + i);
                }
                Log.d("position" + position, "  count" + count + "  length" + length);
                count += 1;
                if (count < length) {
                    imageView.setImageResource(imgs.get(count));
                    this.cancel();
                    this.start();
                }
                else {
                    ViewPager viewPager = ((ViewPager) container);
                    for(RoundCornerProgressBar bar : Progressbars.get(position))
                        bar.setProgress(0);
                    if (position + 1 < getCount()) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                        instantiateItem(container, position+1);
                    }
                }
            }
        };

        if (position == viewPager.getCurrentItem()) {
            Log.d ("And", viewPager.getCurrentItem()+"");
            countDownTimer.start();
        }

        for (int i=0; i < length; i++) {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    8,
                    1.0f
            );
            LayoutInflater layoutInflater1 = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View progressBar2 = layoutInflater1.inflate(R.layout.progressbar, container, false);
            final RoundCornerProgressBar progressbar2 = (RoundCornerProgressBar) progressBar2.findViewById(R.id.progressbar);
            progressbar2.setProgress(0);
            progressbar2.setPadding(10,0,10,0);
            progressbar2.setLayoutParams(param);
            lin.addView(progressbar2);
            progress.add(progressbar2);
        }
        Progressbars.add(progress);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        ((ViewPager)container).removeView((View)object);
    }

}