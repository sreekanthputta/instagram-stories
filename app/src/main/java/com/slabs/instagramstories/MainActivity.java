package com.slabs.instagramstories;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.slabs.instagramstories.transformer.CubeOutTransformer;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ViewPagerAdapter adapter;
    ViewPager pager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        pager = (ViewPager)findViewById(R.id.pager);
        adapter = new ViewPagerAdapter(getApplicationContext(),MainActivity.this,getData());
        pager.setAdapter(adapter);
        pager.setPageTransformer(true,new CubeOutTransformer());
    }

    private List<PlatformData> getData() {
        List<PlatformData> lstResult = new ArrayList<>();
        PlatformData data = new PlatformData();

        data.name = "Android";
        data.addImage(R.drawable.android);
        data.addImage(R.drawable.win10);
        data.addImage(R.drawable.ios);
        lstResult.add(data);

        data = new PlatformData();
        data.name = "iOS";
        data.addImage(R.drawable.one);
        data.addImage(R.drawable.two);
        data.addImage(R.drawable.three);
        lstResult.add(data);

        data = new PlatformData();
        data.name = "UWP";
        data.addImage(R.drawable.four);
        lstResult.add(data);

        return lstResult;
    }
}