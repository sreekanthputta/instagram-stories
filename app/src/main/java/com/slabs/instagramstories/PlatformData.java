package com.slabs.instagramstories;

import java.util.ArrayList;

/**
 * Created by Sreekanth Putta on 04-05-2017.
 */
public class PlatformData {
    public ArrayList<Integer> images= new ArrayList<Integer>();
    public String name;

    public PlatformData() {
    }

    public PlatformData(ArrayList<Integer> images, String name) {
        this.images= images;
        this.name = name;
    }

    public ArrayList<Integer> getImages() {
        return images;
    }

    public void setImages(ArrayList<Integer> images) {
        this.images = images;
    }

    public void addImage(int imageId){
        this.images.add(imageId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
