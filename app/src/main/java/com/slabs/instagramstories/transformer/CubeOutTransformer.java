package com.slabs.instagramstories.transformer;

import android.view.View;

/**
 * Created by Sreekanth Putta on 04-05-2017.
 */
public class CubeOutTransformer extends ABaseTransformer{
    @Override
    protected void onTransform(View view, float position) {
        view.setPivotX(position < 0f ? view.getWidth() : 0f);
        view.setPivotY(view.getHeight() * 0.5f);
        view.setRotationY(25f * position);
    }

    @Override
    public boolean isPagingEnabled() {
        return true;
    }

}
